//
//  HomeDatasource.swift
//  TwitterApp
//
//  Created by Gridlle Mac on 14/07/17.
//  Copyright © 2017 Gridlle Mac. All rights reserved.
//

import LBTAComponents

class HomeDataSource: Datasource {
    let user:[User] = {
       let firstUser = User(name: "Raja", userName: "rajareddy2606", bioText: "I have also tried to do a minor change on a file in the project, then push it to the server. After that I could pull the change from the server to my Desktop computer, but there's still no tag when running  git tag on my desktop computer.", image: #imageLiteral(resourceName: "prabas"), isFollowing: true)
        let secondUser = User(name: "Vinoth", userName: "vinoth.m1994", bioText: "I have also tried to do a minor change on a file in the project, then push it to the server. After that I could pull the change from the server to my Desktop computer, but there's still no tag when running  git tag on my desktop computer.", image: #imageLiteral(resourceName: "surya"), isFollowing: false)
        let thirdUser = User(name: "Arun", userName: "arun55220", bioText: "I have also tried to do a minor change on a file in the project, then push it to the server. After that I could pull the change from the server to my Desktop computer, but there's still no tag when running  git tag on my desktop computer.", image: #imageLiteral(resourceName: "arjun"), isFollowing: false)
        let fourthUser = User(name: "Ajith", userName: "ajithreddy1994", bioText: "I have also tried to do a minor change on a file in the project, then push it to the server. After that I could pull the change from the server to my Desktop computer, but there's still no tag when running  git tag on my desktop computer.", image: #imageLiteral(resourceName: "vijay"), isFollowing: true)
        return [firstUser,secondUser,thirdUser,fourthUser]
    }()
    
    override func numberOfSections() -> Int {
        return 2 
    }
    
    override func headerClasses() -> [DatasourceCell.Type]? {
        return [UserHeader.self]
    }
    
    override func footerClasses() -> [DatasourceCell.Type]? {
        return [UserFooter.self]
    }
    
    override func cellClasses() -> [DatasourceCell.Type] {
        return [UsersCell.self,TweetCell.self]
    }
    
    override func numberOfItems(_ section: Int) -> Int {
        return  self.user.count
    }
    
    override func item(_ indexPath: IndexPath) -> Any? {
        return user[indexPath.item]
    }
    
    
    
    
}


