//
//  HomeDataSourceController+NavBar.swift
//  TwitterApp
//
//  Created by Gridlle Mac on 17/07/17.
//  Copyright © 2017 Gridlle Mac. All rights reserved.
//

import UIKit

extension HomeDataSourceontroller {
    func setupNavigationItem() {
        self.setupNavigationTitleView()
        self.setupLeftNavItems()
        self.setupRightNavItems()
    }
    
    private func setupNavigationTitleView() {
        let titleImage = UIImageView(image: #imageLiteral(resourceName: "twitter_title"))
        let frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        titleImage.frame = frame
        titleImage.contentMode = .scaleAspectFit
        navigationItem.titleView = titleImage
        navigationController?.navigationBar.backgroundColor = UIColor.white
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        let navSeparatorView = UIView()
        navSeparatorView.backgroundColor = UIColor(r: 230, g: 230, b: 230)
        view.addSubview(navSeparatorView)
        navSeparatorView.anchor(view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0.5)
    }
    
    private func setupLeftNavItems() {
        let frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let followBtn = UIButton(type: .system)
        followBtn.setImage(#imageLiteral(resourceName: "follow_icon").withRenderingMode(.alwaysOriginal), for: .normal)
        followBtn.frame = frame
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: followBtn)
    }
    
    private func setupRightNavItems() {
        let frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let searchBtn = UIButton(type: .system)
        searchBtn.setImage(#imageLiteral(resourceName: "twitter_search").withRenderingMode(.alwaysOriginal), for: .normal)
        searchBtn.frame = frame
        let composeBtn = UIButton(type: .system)
        composeBtn.setImage(#imageLiteral(resourceName: "compose").withRenderingMode(.alwaysOriginal), for: .normal)
        composeBtn.frame = frame
        navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: composeBtn), UIBarButtonItem(customView: searchBtn)]
    }
}


