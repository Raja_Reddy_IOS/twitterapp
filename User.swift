//
//  User.swift
//  TwitterApp
//
//  Created by Gridlle Mac on 16/07/17.
//  Copyright © 2017 Gridlle Mac. All rights reserved.
//

import UIKit

struct User {
    let name:String
    let userName:String
    let bioText:String
    let image:UIImage
    let isFollowing:Bool
}
