//
//  UserHeaderFooter.swift
//  TwitterApp
//
//  Created by Gridlle Mac on 16/07/17.
//  Copyright © 2017 Gridlle Mac. All rights reserved.
//

import LBTAComponents

class UserHeader: DatasourceCell {
    
    let headerLbl : UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 16)
        lbl.text = "WHO ARE FOLLOW"
        return lbl
    }()
    
    
    override func setupViews() {
        super.setupViews()
        backgroundColor = UIColor.white
        addSubview(headerLbl)
        separatorLineView.isHidden = false
        separatorLineView.backgroundColor = UIColor(r: 230, g: 230, b: 230)
        self.headerLbl.anchor(self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 12, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
}

class UserFooter: DatasourceCell {
    
    let footerLbl : UILabel = {
        let label = UILabel()
        label.text = "Show Me More"
        label.textColor = twitterBlue
        return label
    }()
    override func setupViews() {
        super.setupViews()
        let whiteBackgroundView = UIView()
        whiteBackgroundView.backgroundColor = UIColor.white
        addSubview(whiteBackgroundView)
        whiteBackgroundView.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 14, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        addSubview(footerLbl)
        footerLbl.anchor(self.topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 12, bottomConstant: 14, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
}
