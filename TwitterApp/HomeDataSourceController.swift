//
//  HomeDataSourceController.swift
//  TwitterApp
//
//  Created by Gridlle Mac on 14/07/17.
//  Copyright © 2017 Gridlle Mac. All rights reserved.
//

import LBTAComponents
class HomeDataSourceontroller: DatasourceController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.backgroundColor = UIColor.groupTableViewBackground
        let homeDatasource = HomeDataSource()
        self.datasource = homeDatasource
        self.setupNavigationItem()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if section != 0 {
            return .zero
        }
        return CGSize(width: self.view.frame.width, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if section != 0 {
            return .zero
        }
        return CGSize(width: self.view.frame.width, height: 64)
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if let user = self.datasource?.item(indexPath) as? User {
            let approxomateWidthOfBioTextView = view.frame.width-28-50-2
            let size = CGSize(width: approxomateWidthOfBioTextView, height: 1000)
            let attribute = [NSFontAttributeName:UIFont.systemFont(ofSize: 15)]
            
            let estimatedFrame = NSString(string: user.bioText).boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: attribute, context: nil)
            return CGSize(width: view.frame.width, height: estimatedFrame.height+60.0)
        }
        return CGSize(width: view.frame.width, height: 150)
    }
    
}
