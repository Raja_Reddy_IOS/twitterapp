//
//  File.swift
//  TwitterApp
//
//  Created by Gridlle Mac on 17/07/17.
//  Copyright © 2017 Gridlle Mac. All rights reserved.
//

import LBTAComponents

class TweetCell: DatasourceCell {
    
    let profileImg : UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "logo")
        imageView.layer.cornerRadius = 5
        imageView.clipsToBounds = true
        return imageView
    }()
    
    override func setupViews() {
        super.setupViews()
        
         backgroundColor = UIColor.white
        separatorLineView.isHidden = false
        separatorLineView.backgroundColor = UIColor(r: 230, g: 230, b: 230)
        
        addSubview(profileImg)
        profileImg.anchor(self.topAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 12, leftConstant: 12, bottomConstant: 0, rightConstant: 0, widthConstant: 50, heightConstant: 50)
        
    }
    
    

    
    
}
