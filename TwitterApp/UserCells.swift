//
//  Cells.swift
//  TwitterApp
//
//  Created by Gridlle Mac on 14/07/17.
//  Copyright © 2017 Gridlle Mac. All rights reserved.
//

import LBTAComponents
let twitterBlue = UIColor(r: 61, g: 167, b: 244)

class UsersCell: DatasourceCell {
    override var datasourceItem: Any? {
        didSet {
            guard let user = datasourceItem as? User else {
                return
            }
            nameLbl.text = user.name
            userNameLbl.text = user.userName
            bioTextLbl.text = user.bioText
            profileImg.image = user.image
            if user.isFollowing {
                followBtn.titleLabel?.text = "Following"
            }else {
                followBtn.titleLabel?.text = "Follow"
            }
            
        }
    }
    let profileImg : UIImageView = {
       let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "logo")
        imageView.layer.cornerRadius = 5
        imageView.clipsToBounds = true
        return imageView
    }()
    
    let nameLbl : UILabel = {
        let label = UILabel()
        label.text = "Gridlle Technologies Private Limited"
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textColor = UIColor.black
//        label.backgroundColor = UIColor.green
        return label
    }()
    
    let userNameLbl : UILabel = {
       let label = UILabel()
        label.text = "User Name"
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = UIColor(r: 130, g: 130, b: 130)
        return label
    }()
    
    let bioTextLbl:UILabel = {
       let label = UILabel()
        label.text = "I have also tried to do a minor change on a file in the project, then push it to the server. After that I could pull the change from the server to my Desktop computer, but there's still no tag when running  git tag on my desktop computer."
//        label.backgroundColor = UIColor.yellow
        label.font = UIFont.systemFont(ofSize: 15)
        label.numberOfLines = 0
        return label
    }()
    
    let followBtn : UIButton = {
        let button = UIButton()
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1.0
        button.setTitle("Follow", for: .normal)
        button.layer.borderColor = twitterBlue.cgColor
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setImage(#imageLiteral(resourceName: "follow_icon"), for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        button.imageEdgeInsets = UIEdgeInsets(top: -4, left: -8, bottom: -4, right: 0)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 4, bottom: 0, right: 0)
        button.setTitleColor(twitterBlue, for: .normal)
        return button
    }()
    
    
    override func setupViews() {
        super.setupViews()
        backgroundColor = UIColor.white 
        addSubview(profileImg)
        addSubview(nameLbl)
        addSubview(userNameLbl)
        addSubview(bioTextLbl)
        addSubview(followBtn)
        separatorLineView.isHidden = false
        separatorLineView.backgroundColor = UIColor(r: 230, g: 230, b: 230)
        profileImg.anchor(self.topAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 12, leftConstant: 12, bottomConstant: 0, rightConstant: 0, widthConstant: 50, heightConstant: 50)
        
        self.nameLbl.anchor(self.profileImg.topAnchor, left: self.profileImg.rightAnchor, bottom: nil, right: self.followBtn.leftAnchor, topConstant: 0, leftConstant: 8, bottomConstant: 0, rightConstant: 12, widthConstant: 0, heightConstant: 20)
        
        self.userNameLbl.anchor(self.nameLbl.bottomAnchor, left: self.nameLbl.leftAnchor, bottom: nil, right: self.followBtn.leftAnchor , topConstant: 0
            , leftConstant: 0, bottomConstant: 0, rightConstant: 12, widthConstant: 0, heightConstant: 20)
        
        
        self.bioTextLbl.anchor(self.userNameLbl.bottomAnchor, left: self.userNameLbl.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 0)
        
        self.followBtn.anchor(self.topAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant: 12, leftConstant: 0, bottomConstant: 0, rightConstant: 4, widthConstant: 120, heightConstant: 34)
        
        
    }
    
}
